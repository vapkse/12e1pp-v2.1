/*
12E1 PP
 Version 2.1.4
 Date: 25.04.2015
 */

#include <EasyTransfer.h>
#include <AmpTransfer.h>
#include <Blink.h>
#include <PID.h>
#include <OneWire.h>
#include <DallasTemperature.h>

static const byte PP12E1_ID = 212;
static const byte ampId = PP12E1_ID;

// Pin Config
#define ledPin 2
#define reg2Pin 3
#define screenCurrentPin 4
#define oneWireBusPin 5
#define indicatorPin 6
#define phaseDetectionPin 7
#define relayPin 8
#define reg3Pin 9
#define reg4Pin 10
#define reg1Pin 11
#define modulationPin A0 // Not used on the differential feedback version
#define current2Pin A1
#define current4Pin A2
#define current3Pin A3
#define current1Pin A4
#define switchPin A5

// Constants
#define startCurrent 20 // 0.256 per mA
#define minCurrent 100 // 0.256 per mA
#define maxCurrent 585 // 0.256 per mA
#define minimalRefCurrent 176 // 0.256 per mA
#define maximalRefCurrent 391 // 0.256 per mA
#define dischargeMinTime 1 // Seconds
#define dischargeMaxTime 10 // Seconds
#define heatMaxTime 40 // seconds
#define highVoltageMaxTime 20 // Seconds
#define regulationMaxTime 100 // Seconds
#define outOfRangeMaxTime 300 // Seconds
#define errorMaxTime 500 // Milli-seconds
#define masterP 0.1
#define slaveP 0.2
#define masterI 0.00007
#define slaveI 0.00015
#define functionMasterI 0.000005
#define functionSlaveI 0.000015
#define pidSampleTime 100
#define regulationTreshold 4 // 0.256 per mA
#define stabilizedTreshold 60 // 0.256 per mA
#define functionTreshold 20 // 0.256 per mA
#define currentRatio 3
#define currentAverageRatio 50
#define pidSetPointSlaveRatio 3
#define modulationPercentReductionFactor 0.003
#define classBLevelDetectedMinTime 2000 // Milli-seconds
#define heatThinkTempMax 85 // > 85deg
#define airTempMax 70 // > 70deg
#define indicatorMaxRange 180
#define indicatorCorrectionRatio 1.7
#define phaseDetectionErrorMaxTime 100 // loop count, max 255
#define screenCurrentErrorMaxTime 300 // Milli-seconds
#define diagnosticSendMinTime 200 // Milli-seconds
#define tempMeasureMinTime 3000 // Milli-seconds
#define classBAverageRatio 10

// Internal use
Blink led;
unsigned long lastDiagnosticTime = 0;
unsigned long lastTempMeasureTime = 0;
OneWire oneWire(oneWireBusPin);
DallasTemperature tempSensors(&oneWire);
unsigned int measure1 = 0;
unsigned int measure2 = 0;
unsigned int measure3 = 0;
unsigned int measure4 = 0;
double current1 = 0;
double current2 = 0;
double current3 = 0;
double current4 = 0;
double current1Average = 0;
double current2Average = 0;
double current3Average = 0;
double current4Average = 0;
double classBLevelAverage; //Initialized on reset()
double modulationPercent = 0; //Initialized on reset()
unsigned long classBLevelDetectedTime = 0;
double pid1Output; //Initialized on resetRegulators()
double pid2Output; //Initialized on resetRegulators()
double pid3Output; //Initialized on resetRegulators()
double pid4Output; //Initialized on resetRegulators()
byte percentageSetPoint; //Initialized on reset()
double pidSetPoint; //Initialized on reset()
double pid2SetPoint; //Initialized on reset()
double pid4SetPoint; //Initialized on reset()
unsigned int stepMaxTime = 0;
unsigned int stepElapsedTime = 0;
unsigned int stepMaxValue = 0;
unsigned int stepCurValue = 0;
unsigned int airTemp = 0;
unsigned int powerSupply1Temp = 0;
unsigned int powerSupply2Temp = 0;
unsigned long sequenceStartTime; //Initialized on reset()
unsigned long outOfRangeTime; //Initialized on reset()
unsigned long errorTime; //Initialized on reset()
byte phaseDetectionErrorCount = 0;
unsigned long screenCurrentErrorStartTime = 0;
boolean relayState; //Initialized on reset()
byte relayClockState = LOW;

// Init regulators
PID pid1(&current1Average, &pid1Output, &pidSetPoint, masterP, masterI, 0, 0, 255, pidSampleTime, true);
PID pid2(&current2Average, &pid2Output, &pid2SetPoint, slaveP, slaveI, 0, 0, 255, pidSampleTime, true);
PID pid3(&current3Average, &pid3Output, &pidSetPoint, masterP, masterI, 0, 0, 255, pidSampleTime, true);
PID pid4(&current4Average, &pid4Output, &pid4SetPoint, slaveP, slaveI, 0, 0, 255, pidSampleTime, true);

// Timers definition
#define TIMER_CLK_DIV1 0x01 // Timer clocked at F_CPU
#define TIMER_PRESCALE_MASK 0x07
#define WATCHDOG_DELAY_16MS 0

// Temp indexes in one wire bus
#define AIR_TEMPERATURE 0
#define POWERSUPPLY1_TEMPERATURE 1
#define POWERSUPPLY2_TEMPERATURE 2

// Sequence:
#define SEQ_DISCHARGE 0 // 0: Discharge
#define SEQ_HEAT 1 // 1: Heat tempo
#define SEQ_STARTING 2 // 2: Starting High Voltage
#define SEQ_REGULATING 3 // 3: Waiting for reg
#define SEQ_FUNCTION 4 // 4: Normal Function
#define SEQ_FAIL 5 // 5: Fail
byte sequence = SEQ_DISCHARGE;

// Errors
#define NO_ERR 0 // No error
#define ERR_DISHARGETOOLONG 2 // 2: Discharge too long
#define ERR_CURRENTONHEAT 3 // 3: Current during heat time
#define ERR_STARTINGOUTOFRANGE 4 // 4: Out of range during starting
#define ERR_REGULATINGTOOLONG 5 // 5: Regulation too long
#define ERR_REGULATINGMAXREACHED 6 // 6: Maximun reached during regulation
#define ERR_REGULATINGMINREACHED 7 // 7: Minimum reached during regulation
#define ERR_FUNCTIONMAXREACHED 8 // 8: Maximun reached during normal function
#define ERR_FUNCTIONMINREACHED 9 // 9: Minimum reached during normal function
#define ERR_FUNCTIONOUTOFRANGE 10 // 10: Time elapsed with current out of range during normal function
#define ERR_STARTINGTOOLONG 11 // 11: Starting too long
#define ERR_TEMPTOOHIGH 12 // 12: Temperature maximum reached
#define ERR_SCREENCURRENTTOOHIGH 13 // 13: Screen current too high
#define ERR_PHASE 14 // 14: Phase detection error
byte errorNumber = NO_ERR;

#define ERR_TUBE_1 1
#define ERR_TUBE_2 2
#define ERR_TUBE_3 3
#define ERR_TUBE_4 4
#define ERR_TUBE_5 5
#define ERR_TUBE_6 6
#define ERR_TUBE_7 7
#define ERR_TUBE_8 8
#define ERR_TEMP_AIR 1
#define ERR_TEMP_PS1 2
#define ERR_TEMP_PS2 3
byte errorCause = NO_ERR;
boolean displayTubeNumber = false;

#define CHECK_RANGE_OK 0
#define CHECK_RANGE_TOOLOW 1
#define CHECK_RANGE_TOOHIGH 2

// Diagnostic
EasyTransfer dataTx;
dataResponse dataTxStruct;

void reset(){
  sequenceStartTime = 0;
  outOfRangeTime = 0;
  modulationPercent = 0;
  percentageSetPoint = 0;
  pidSetPoint = minimalRefCurrent;
  pid2SetPoint = 0;
  pid4SetPoint = 0;
  classBLevelAverage = 0;
  resetRegulators();
}

void resetRegulators(){
  relayState = false;

  pid1.SetEnabled(false);
  pid2.SetEnabled(false);
  pid3.SetEnabled(false);
  pid4.SetEnabled(false);

  pid1Output = 255;
  pid2Output = 255;
  pid3Output = 255;
  pid4Output = 255;

  analogWrite(reg1Pin, 255);
  analogWrite(reg2Pin, 255);
  analogWrite(reg3Pin, 255);
  analogWrite(reg4Pin, 255);
}

void initRegulators()
{
  if (sequence == SEQ_FUNCTION){
    pid1.SetGains(0, functionMasterI, 0);
    pid2.SetGains(0, functionSlaveI, 0);
    pid3.SetGains(0, functionMasterI, 0);
    pid4.SetGains(0, functionSlaveI, 0);
  }
  else{
    pid1.SetGains(masterP, masterI, 0);
    pid2.SetGains(slaveP, slaveI, 0);
    pid3.SetGains(masterP, masterI, 0);
    pid4.SetGains(slaveP, slaveI, 0);
  }

  pid1.SetEnabled(true);
  pid2.SetEnabled(true);
  pid3.SetEnabled(true);
  pid4.SetEnabled(true);
}

void computeRegulators()
{
  if (pid1.Compute()) {
    analogWrite(reg1Pin, constrain((int)pid1Output, 0, 255));
  }
  if (pid2.Compute()) {
    analogWrite(reg2Pin, constrain((int)pid2Output, 0, 255));
  }
  if (pid3.Compute()) {
    analogWrite(reg3Pin, constrain((int)pid3Output, 0, 255));
  }
  if (pid4.Compute()) {
    analogWrite(reg4Pin, constrain((int)pid4Output, 0, 255));
  }
}

byte checkInRange(double minValue, double maxValue)
{
  if (minValue > 0){
    if (current1Average < minValue)
    {
      errorCause = ERR_TUBE_1;
      return CHECK_RANGE_TOOLOW;
    }
    if (current2Average < minValue)
    {
      errorCause = ERR_TUBE_2;
      return CHECK_RANGE_TOOLOW;
    }
    if (current3Average < minValue)
    {
      errorCause = ERR_TUBE_3;
      return CHECK_RANGE_TOOLOW;
    }
    if (current4Average < minValue)
    {
      errorCause = ERR_TUBE_4;
      return CHECK_RANGE_TOOLOW;
    }
  }

  if (maxValue > 0){
    if (current1Average > maxValue)
    {
      errorCause = ERR_TUBE_1;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current2Average > maxValue)
    {
      errorCause = ERR_TUBE_2;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current3Average > maxValue)
    {
      errorCause = ERR_TUBE_3;
      return CHECK_RANGE_TOOHIGH;
    }
    if (current4Average > maxValue)
    {
      errorCause = ERR_TUBE_4;
      return CHECK_RANGE_TOOHIGH;
    }
  }

  errorCause = NO_ERR;
  return CHECK_RANGE_OK;
}

unsigned int calcRegulationProgress(double minValue, double maxValue, double range)
{
  double percentProgress = 100;

  if (current1Average < minValue)
  {
    percentProgress = 100 * (1 - (minValue - current1Average) / range);
  }
  else if (current1Average > maxValue)
  {
    percentProgress = 100 * (1 - (current1Average - maxValue) / range);
  }

  if (current2Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current2Average) / range), percentProgress);
  }
  else if (current2Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current2Average - maxValue) / range), percentProgress);
  }

  if (current3Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current3Average) / range), percentProgress);
  }
  else if (current3Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current3Average - maxValue) / range), percentProgress);
  }

  if (current4Average < minValue)
  {
    percentProgress = min(100 * (1 - (minValue - current4Average) / range), percentProgress);
  }
  else if (current4Average > maxValue)
  {
    percentProgress = min(100 * (1 - (current4Average - maxValue) / range), percentProgress);
  }

  return constrain((int)percentProgress, 0, 100);
}

void sendDatas()
{
  // Send datas
  dataTxStruct.message = MESSAGE_SENDVALUES;
  dataTxStruct.step = sequence;
  dataTxStruct.stepMaxTime = stepMaxTime;
  dataTxStruct.stepElapsedTime = stepElapsedTime;
  dataTxStruct.stepMaxValue = stepMaxValue;
  dataTxStruct.stepCurValue = stepCurValue;
  dataTxStruct.tickCount = millis();
  dataTxStruct.measure0 = map(current1Average, 0, 1023, 0, 255); // Input 1024 max, but I transfer only a range of 255
  dataTxStruct.measure1 = map(current2Average, 0, 1023, 0, 255);
  dataTxStruct.measure2 = map(current3Average, 0, 1023, 0, 255);
  dataTxStruct.measure3 = map(current4Average, 0, 1023, 0, 255);
  dataTxStruct.measure4 = constrain((int)(classBLevelAverage * 10), 0, 255);
  dataTxStruct.measure5 = map((int)modulationPercent, 0, 100, 0, 255);
  dataTxStruct.output0 = 255 - constrain((int)pid1Output, 0, 255);
  dataTxStruct.output1 = 255 - constrain((int)pid2Output, 0, 255);
  dataTxStruct.output2 = 255 - constrain((int)pid3Output, 0, 255);
  dataTxStruct.output3 = 255 - constrain((int)pid4Output, 0, 255);
  dataTxStruct.temperature0 = constrain(airTemp, 0, 255);
  dataTxStruct.temperature1 = constrain(powerSupply1Temp, 0, 255);
  dataTxStruct.temperature2 = constrain(powerSupply2Temp, 0, 255);
  dataTxStruct.minValue = map(minCurrent, 0, 1023, 0, 255);
  dataTxStruct.refValue = map(pidSetPoint, 0, 1023, 0, 255);
  dataTxStruct.maxValue = map(maxCurrent, 0, 1023, 0, 255);
  dataTxStruct.errorNumber = errorNumber;
  dataTxStruct.errorTube = errorCause;
  dataTx.sendData();

  if (modulationPercent > modulationPercentReductionFactor ){
    modulationPercent -= modulationPercentReductionFactor;
  }
  else {
    modulationPercent = 0;
  }

  lastDiagnosticTime = millis();
}

void measureTemperatures()
{
  // Send the command to get temperatures
  tempSensors.requestTemperatures();
  airTemp = tempSensors.getTempCByIndex(AIR_TEMPERATURE);
  powerSupply1Temp = tempSensors.getTempCByIndex(POWERSUPPLY1_TEMPERATURE);
  powerSupply2Temp = tempSensors.getTempCByIndex(POWERSUPPLY2_TEMPERATURE);
  lastTempMeasureTime = millis();
}

void relayOn()
{
  relayState = true;
}

void relayOff()
{
  relayState = false;
}

void regulate(){
  // Reset all elapsed time and force regulation
  sequenceStartTime = 0;
  sequence = SEQ_REGULATING;
}

void displayIndicator()
{
  displayTubeNumber = false;
  unsigned int value = 0;
  // Limits 0,56,180,316,432
  int switchValue = analogRead(switchPin);
  if (switchValue < 30)
  {
    // Vu-meter read working left position 1
    value = current1Average;
  }
  else if (switchValue < 100)
  {
    // Vu-meter read working left position 2
    value = current2Average;
  }
  else if (switchValue < 270)
  {
    // Vu-meter is off
    value = 0;
    displayTubeNumber = true;
  }
  else if (380)
  {
    // Vu-meter read working left position 3
    value = current3Average;
  }
  else
  {
    // Vu-meter read working left position 4
    value = current4Average;
  }

  double displayValue = value * indicatorCorrectionRatio;
  analogWrite(indicatorPin, map(constrain((int)displayValue, 0, 1023), 0, 1023, 0, indicatorMaxRange));
}

byte calcPercentSetPoint(){
  if (modulationPercent > 80) {
    return 100;
  }
  if (modulationPercent > 60) {
    return 75;
  }
  if (modulationPercent > 40) {
    return 50;
  }
  if (modulationPercent > 20) {
    return 25;
  }
  return 0;
}

void setupWatchdog(unsigned int timeLaps) {
  byte bb;
  if (timeLaps > 9 ){
    timeLaps = 9;
  }
  bb = timeLaps & 7;
  if (timeLaps > 7){
    bb |= (1 << 5);
  }
  bb |= (1 << WDCE);

  MCUSR &= ~(1 << WDRF);
  // start timed sequence
  WDTCSR |= (1 << WDCE) | (1 << WDE);
  // set new watchdog timeout value
  WDTCSR = bb;
  WDTCSR |= _BV(WDIE);
}

// Watchdog Interrupt Service / is executed when watchdog timed out
ISR(WDT_vect) {
  if (relayState) {
    relayClockState = relayClockState == HIGH ? LOW : HIGH;
    digitalWrite(relayPin, relayClockState);
  }
}

void setup() {
  // initialize the digital pin as an output.
  pinMode(ledPin, OUTPUT);
  pinMode(relayPin, OUTPUT);
  pinMode(oneWireBusPin, OUTPUT);
  pinMode(phaseDetectionPin, INPUT_PULLUP);
  pinMode(screenCurrentPin, INPUT);

  // Set pwm speed
  TCCR1B = (TCCR1B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
  TCCR2B = (TCCR2B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;

  digitalWrite(oneWireBusPin, HIGH);

  reset();

  setupWatchdog(WATCHDOG_DELAY_16MS);

  led.Setup(ledPin, false);

  tempSensors.begin();
  tempSensors.requestTemperatures();
  measureTemperatures();

  // Diagnostic
  Serial.begin(9600);
  dataTxStruct.id = ampId;
  dataTx.begin(details(dataTxStruct), &Serial);

  analogReference(INTERNAL);
}

void loop()
{
  unsigned int elapsedTime;
  unsigned int check;
  unsigned long currentTime = millis();

  //checkPhase
  if (sequence != SEQ_FAIL && digitalRead(phaseDetectionPin) == HIGH){
    phaseDetectionErrorCount++;
    if (phaseDetectionErrorCount > phaseDetectionErrorMaxTime){
      // Fail, phase error
      sequence = SEQ_FAIL;
      errorNumber = ERR_PHASE;
    }
  }
  else{
    phaseDetectionErrorCount = 0;
  }

  if (sequence >= SEQ_STARTING && digitalRead(screenCurrentPin) == LOW){
    if (screenCurrentErrorStartTime == 0){
      screenCurrentErrorStartTime = currentTime;
    }
    else if (currentTime - screenCurrentErrorStartTime > screenCurrentErrorMaxTime){
      // Fail, screen current error
      sequence = SEQ_FAIL;
      errorNumber = ERR_SCREENCURRENTTOOHIGH;
    }
  }
  else{
    screenCurrentErrorStartTime = 0;
  }

  if (sequence != SEQ_FAIL){
    if (airTemp > airTempMax) {
      // Fail, air temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_AIR;
    }
    else if (powerSupply1Temp > heatThinkTempMax) {
      // Fail, regulators 1 temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_PS1;
    }
    else if (powerSupply2Temp > heatThinkTempMax) {
      // Fail, regulators 2 temperature too high
      sequence = SEQ_FAIL;
      errorNumber = ERR_TEMPTOOHIGH;
      errorCause = ERR_TEMP_PS2;
    }
    else {
      // Read the input
      measure1 = analogRead(current1Pin);
      measure2 = analogRead(current2Pin);
      measure3 = analogRead(current3Pin);
      measure4 = analogRead(current4Pin);

      // First average for classBLevel measure
      current1 += (measure1-current1)/currentRatio;
      current2 += (measure2-current2)/currentRatio;
      current3 += (measure3-current3)/currentRatio;
      current4 += (measure4-current4)/currentRatio;

      // Second average for regulation
      current1Average += (current1-current1Average)/currentAverageRatio;
      current2Average += (current2-current2Average)/currentAverageRatio;
      current3Average += (current3-current3Average)/currentAverageRatio;
      current4Average += (current4-current4Average)/currentAverageRatio;

      if (sequence == SEQ_FUNCTION) {
        classBLevelAverage += (max(measure3 + measure4 - current3 - current4, 0) + max(measure1 + measure2 - current1 - current2, 0) - classBLevelAverage)/classBAverageRatio;
        if (classBLevelAverage > 3){
          sendDatas();
          // Two overshoot in classBLevelDetectedMinTime are required to boost the current. Prevent glitch due to a sector pollution.
          if (currentTime - classBLevelDetectedTime < classBLevelDetectedMinTime){
            modulationPercent += classBLevelAverage;

            if (modulationPercent > 100) {
              modulationPercent = 100;
            }
          }

          classBLevelDetectedTime = currentTime;
          classBLevelAverage = 0;
        }
      }

      // Calc regulators set point
      byte percentage = calcPercentSetPoint();
      if (percentageSetPoint != percentage){
        // New set point must be set
        pidSetPoint = percentage * (maximalRefCurrent - minimalRefCurrent) / 100 + minimalRefCurrent;
        percentageSetPoint = percentage;
        if (sequence >= SEQ_STARTING){
          regulate();
        }
      }

      // Average set points for slave regulators from master measure
      pid2SetPoint += (current1Average-pid2SetPoint)/pidSetPointSlaveRatio;
      pid4SetPoint += (current3Average-pid4SetPoint)/pidSetPointSlaveRatio;

      displayIndicator();
    }
  }

  switch (sequence)
  {
  case SEQ_DISCHARGE:
    // Discharging

    // Reset errors
    errorCause = NO_ERR;
    errorNumber = NO_ERR;

    // Pre-sequence
    if (sequenceStartTime == 0){
      resetRegulators();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    led.Execute(800, 200);

    // Diagnostic
    stepMaxTime = dischargeMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = 0;
    stepCurValue = 1;

    if(elapsedTime > dischargeMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_DISHARGETOOLONG;
      break;
    }

    if(elapsedTime < dischargeMinTime || checkInRange(0, startCurrent) == CHECK_RANGE_TOOHIGH)
    {
      break;
    }

    // Post-sequence
    sendDatas();
    sequenceStartTime = 0;
    sequence++;

  case SEQ_HEAT:
    // Startup tempo

    // Pre-sequence
    if (sequenceStartTime == 0){
      resetRegulators();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    led.Execute(400, 400);

    // Diagnostic
    stepMaxTime = heatMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = heatMaxTime;
    stepCurValue = elapsedTime;

    if(checkInRange(0, startCurrent + 10) == CHECK_RANGE_TOOHIGH)
    {
      // Fail, no current allowed now
      sequence = SEQ_FAIL;
      errorNumber = ERR_CURRENTONHEAT;
      break;
    }

    if(elapsedTime < heatMaxTime)
    {
      break;
    }

    // Diagnostic, force 100%
    stepElapsedTime = heatMaxTime;
    stepCurValue = heatMaxTime;

    // Post-sequence
    sequenceStartTime = 0;
    led.On();
    measureTemperatures();
    sendDatas();
    sequence++;
    delay(250);

  case SEQ_STARTING:
    // Starting High Voltage

    // Pre-sequence
    if (sequenceStartTime == 0){
      relayOn();
      initRegulators();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    // Regulation
    computeRegulators();

    led.Execute(20, 400);

    // Diagnostic
    stepMaxTime = highVoltageMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = 100;
    stepCurValue = calcRegulationProgress(pidSetPoint/2, pidSetPoint + stabilizedTreshold, pidSetPoint - stabilizedTreshold);

    if(elapsedTime > highVoltageMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_STARTINGTOOLONG;
      break;
    }

    if (checkInRange(0, maxCurrent) != CHECK_RANGE_OK)
    {
      // Fail current error
      sequence = SEQ_FAIL;
      errorNumber = ERR_STARTINGOUTOFRANGE;
      break;
    }

    // If target points not reached, continue to regulate
    if(elapsedTime < 5 || checkInRange(pidSetPoint/2, pidSetPoint + stabilizedTreshold) != CHECK_RANGE_OK)
    {
      break;
    }

    // Post-sequence
    sequenceStartTime = 0;
    sendDatas();
    sequence++;

  case SEQ_REGULATING:
    // Waiting for reg

    // Pre-sequence
    if (sequenceStartTime == 0){
      relayOn();
      initRegulators();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    // Regulation
    computeRegulators();

    led.Execute(20, 1500);

    // Diagnostic
    stepMaxTime = regulationMaxTime;
    stepElapsedTime = elapsedTime;
    stepMaxValue = 100;
    stepCurValue = calcRegulationProgress(pidSetPoint - regulationTreshold, pidSetPoint + regulationTreshold, pidSetPoint - regulationTreshold);

    if(elapsedTime > regulationMaxTime)
    {
      // Fail, too late
      sequence = SEQ_FAIL;
      errorNumber = ERR_REGULATINGTOOLONG;
      break;
    }

    if (checkInRange(0, maxCurrent) != CHECK_RANGE_OK)
    {
      // Fail current error
      if (errorTime == 0){
        errorTime = currentTime;
      }

      if (currentTime - errorTime > errorMaxTime)
      {
        // Fail current error
        sequence = SEQ_FAIL;
        errorNumber = ERR_REGULATINGMAXREACHED;
        break;
      }
    } 
    else {
      errorTime = 0;
    }

    // If target points not reached, continue to regulate
    if(checkInRange(pidSetPoint - regulationTreshold, pidSetPoint + regulationTreshold) != CHECK_RANGE_OK)
    {
      break;
    }

    // Post-sequence
    sendDatas();
    sequenceStartTime = 0;
    sequence++;

  case SEQ_FUNCTION:
    // Normal Function

    // Pre-sequence
    if (sequenceStartTime == 0){
      relayOn();
      initRegulators();
      led.Off();
      sequenceStartTime = currentTime;
      lastDiagnosticTime = 0;
      elapsedTime = 0;
    }
    else {
      // Calc elapsed time in seconds
      elapsedTime = (currentTime - sequenceStartTime) / 1000;
    }

    // Regulation
    computeRegulators();

    // Measure Temp
    if (currentTime - lastTempMeasureTime > tempMeasureMinTime){
      measureTemperatures();
    }

    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = elapsedTime;
    stepMaxValue = 0;
    stepCurValue = 0;

    check = checkInRange(minCurrent, maxCurrent);
    if(check != CHECK_RANGE_OK)
    {
      if (errorTime == 0){
        errorTime = currentTime;
      }

      if (currentTime - errorTime > errorMaxTime)
      {
        // Fail current error
        sequence = SEQ_FAIL;
        errorNumber = check == CHECK_RANGE_TOOLOW ? ERR_FUNCTIONMINREACHED : ERR_FUNCTIONMAXREACHED;
        break;
      }
    } 
    else {
      errorTime = 0;
    }

    if(checkInRange(pidSetPoint - functionTreshold, pidSetPoint + functionTreshold) != CHECK_RANGE_OK)
    {
      if (outOfRangeTime == 0){
        outOfRangeTime = currentTime;
      }

      if ((currentTime - outOfRangeTime) / 1000 > outOfRangeMaxTime)
      {
        // Fail out of range error
        sequence = SEQ_FAIL;
        errorNumber = ERR_FUNCTIONOUTOFRANGE;
        break;
      }
    }
    else{
      outOfRangeTime = 0;
    }
    break;

  default:
    // Fail, protect mode
    relayOff();

    // Diagnostic
    stepMaxTime = 0;
    stepElapsedTime = 0;
    stepMaxValue = 0;
    stepCurValue = 0;
    lastDiagnosticTime = 0;

    // Error indicator
    if (errorNumber == ERR_PHASE){
      // Fast blinking on phase error (Stress blinking)
      led.Execute(80, 80);
    }
    else {
      // Otherwise display error number or tube number
      led.Execute(250, displayTubeNumber ? errorCause : errorNumber, 1200);
    }
  }

  // Diagnostic
  if (currentTime - lastDiagnosticTime > diagnosticSendMinTime) {
    sendDatas();
  }
}





